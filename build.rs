use std::fs;
use std::io::Write;
use std::path;

use pulldown_cmark::{Event, HeadingLevel, Parser, Tag};

fn build_dir<P: AsRef<path::Path>>(dir : P) {
  let md_files = fs::read_dir(dir).unwrap().map(Result::unwrap).filter(|entry| entry.path().extension().unwrap() == "md");

  for md_file in md_files {
    let file_contents = fs::read_to_string(md_file.path()).unwrap();
    let mut md_parser = Parser::new(&file_contents).peekable();

    // Quick and dirty parser, expecting a given structure
    macro_rules! match_tags {
      ($tag_pattern:pat) => {
        {
          let mut lines = vec![];
          match md_parser.next() {
            Some(Event::Start($tag_pattern)) => (),
            any@_ => panic!("Unexepcted event {:?}", any),
          }
          while let Some(Event::Text(text)) = md_parser.next_if(
            |event| match event { Event::Text(_) => true, _ => false }) {

            lines.push(text.into_string());
          }

          match md_parser.next() {
            Some(Event::End($tag_pattern)) => (),
            any@_ => panic!("Unexepcted event {:?}", any),
          }

          lines
        }
      }
    }

    let _header = match_tags!(Tag::Heading(HeadingLevel::H1, _, _));
    let code = match_tags!(Tag::CodeBlock(_));

    let mut code_file = fs::File::create(md_file.path().with_extension("rs")).unwrap();
    for line in code {
      write!(&mut code_file, "{}\n", line).unwrap();
    };
  }
}

fn main() {
  build_dir("tests/passing");
  build_dir("tests/failing");
  build_dir("tests/possible");
}
