# Borrowck-examples

This repository serves as a set of compiler-checked examples of Rust programs
which exhibit "interesting" borrow-checking behaviour, categorised by whether
they pass the borrow checker or not.

The `tests` folder contains Markdown files which give the test name, the
relevant code, and a short description or explanation if any. When running
the tests, the code blocks are converted into Rust programs then checked by
the compiler.
