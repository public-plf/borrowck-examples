extern crate compiletest_rs as compiletest;

use compiletest_rs::common::Mode;

use std::path::PathBuf;

fn run_dir(dir: &'static str, mode: Mode) {
  let mut config = compiletest::Config::default();

  config.mode = mode;
  config.src_base = PathBuf::from(format!("tests/{}", dir));
  config.link_deps(); // Populate config.target_rustcflags with dependencies on the path
  config.clean_rmeta(); // If your tests import the parent crate, this helps with E0464

  compiletest::run_tests(&config);
}

#[test]
fn compile_test() {
  run_dir("passing", Mode::RunPass);
  run_dir("failing", Mode::CompileFail);
  run_dir("possible", Mode::CompileFail);
}

