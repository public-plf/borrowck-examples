# Function constraint failure from reborrow termination

```rust
fn foo<'a, 'b>(x : &'a mut &'b mut i32, y : &'b mut i32) {
  () // For example: *x = y;
}

fn bar<'a, 'b>(u : &'a mut &'b mut i32, v : &'b mut i32) {
  foo(u, &mut *v);
  *v = 1; //~ ERROR
}

fn main() {}
```
