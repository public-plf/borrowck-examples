# Liveness linking through unification

```rust
fn main() {
  let mut x = (0, 1);
  let (y, z) = if true {
    (&mut x.0, &mut x.1)
  } else {
    (&mut x.1, &mut x.0)
  };
  x.0 = 2; //~ ERROR cannot assign to `x.0` because it is borrowed
  let w = *y;
}
```
