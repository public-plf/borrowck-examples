# Two-way liveness linking through function signature

```rust
fn foo<'a, 'b, 'c>
  ( x : &'a mut &'b mut &'c mut i32 
  , y : &'b mut &'c mut i32) {
  () // Or for example: *x = y;
}

fn main() {
  let mut x = 0;
  let mut y = 1;
  
  let mut a = &mut x;
  let mut b = &mut y;
  
  let mut i = &mut a;
  let mut j = &mut b;
  
  let v = &mut i;
  foo(&mut *v, &mut *j);
  
  let mut z = 2;
  **v = &mut z;
  
  z = 3; //~ ERROR cannot assign to `z` because it is borrowed
  **j = 4;
}
```
