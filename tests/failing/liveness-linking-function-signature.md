# Liveness linking through function signature

```rust
fn foo<'a, 'b>(x : &'a mut &'b i32, y : &'b i32) {
  () // Or for example: *x = y
}

fn main() {
  let x = 0;
  let mut y = 1;

  let mut a = &x;
  let b = &y;

  let v = &mut a;
  foo(&mut *v, b);

  y = 2; //~ ERROR cannot assign to `y` because it is borrowed
  let w = **v;
}
```
