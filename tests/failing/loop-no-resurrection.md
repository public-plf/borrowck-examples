# Borrows in loops don't resurrect 

```rust
fn main() {
  let mut x = 0;
  let mut y = &x;
  let z = y;
  while (*y < 10) {
    x += 1; //~ ERROR
    y = &x;
  }
  let w = *z;
}
```
