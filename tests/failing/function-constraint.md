# Function constraint failure

```rust
fn foo<'a, 'b>(x : &'a i32, y : &'b i32) -> &'a i32{
  y //~ ERROR
}

fn main() {}
```
