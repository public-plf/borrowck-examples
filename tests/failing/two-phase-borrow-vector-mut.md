# Two-phase borrowing abuse

```rust
fn main() {
  let mut v : Vec<Vec<usize>> = vec![vec![0]];
  v[0].push({ v.push(vec![1]); 1 });
  //~^ ERROR cannot borrow `v` as mutable more than once at a time
}
```

A modification of the two-phase RFC example. Now, an element of the vector is
reserved - but that reservation must clash with the attempt to mutably borrow
`v` in the inner call to `push`, since otherwise `v` could be reallocated and
the receiver reference could be invalidated.

This goes to show that it's not enough to simply consider the receiver
reference "last" in the borrow-checking logic. It really has to be created
first, and activated later.
