# Nested mutable reference linking

```rust
fn main() {
  let mut x = 0;
  let mut y = 1;
  
  let mut a = &mut x;
  let b = &mut y;
  
  let i = &mut a;
  *i = b;
  
  x = 2; //~ ERROR cannot assign to `x` because it is borrowed
  let w = **i;
}
```
