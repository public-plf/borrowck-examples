# Function constraint failure from a local variable

```rust
fn foo<'a, 'b>(x : &'a mut &'b mut i32, y : &'b mut i32) {
  () // For example: *x = y;
}

fn bar<'a, 'b>(u : &'a mut &'b mut i32) {
  let mut v = 0;
  foo(u, &mut v); //~ ERROR
}

fn main() {}
```
