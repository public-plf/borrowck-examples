# Liveness linking from loop overapproximation

```rust
fn main() {
  let mut x = 0;
  let mut y = 1;
  let mut a = &mut x;
  while (*a == 0) {
    a = &mut y;
  }
  
  x = 2; //~ ERROR cannot assign to `x` because it is borrowed
  *a = 3;
}
```
