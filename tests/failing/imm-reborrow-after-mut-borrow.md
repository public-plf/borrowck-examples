# Mutable borrow over immutable reborrow

```rust
fn main() {
    let x : (Box<i32>, Box<i32>) = (Box::new(0i32), Box::new(1i32));
    let mut y = &x;
    let w = &mut y;
    let z = &(*y.1); //~ ERROR cannot borrow `*y.1` as immutable
    println!("{}, {}", (*w).0, *z);
}
```

This example works if `z` is created *before* `w`, but since it is created
afterwards, the borrows conflict.

It's unclear to me if this is possibly admissible, or if there is a latent race
condition in trying to admit this program, since the contents of `y.1` is known
to be immutable for the lifetime of `y` even under the borrow by `w`.
