# Fresh borrows inside loops

```rust
fn main() {
  let mut x = 0;
  let mut y = &mut x;
  let z = &mut y;
  while(**z < 10) {
      x = x + 1; //~ ERROR cannot assign to `x` because it is borrowed
                 //~^ ERROR cannot use `x` because it was mutably borrowed
      *z = &mut x;
  }
  **z = 0;
}
```
Unlike the case where `y` is simply assigned a new borrow every loop, this
program is rightly rejected by the borrow checker since the termination of `y` 
invalidates `z`, and so makes it un-reassignable.

Completely reassigning `z = &mut &mut x` would still be allowed, however. Note
that this transformation isn't possible if `z` comes out of e.g. a function
argument, which is why the above code isn't sound in general.
