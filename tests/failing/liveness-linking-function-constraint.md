# Liveness linking through function constraints

```
fn foo<'a, 'b : 'a>(x : &'a i32, y : &'b i32) -> &'a i32 {
  x // Or y
}

fn main() {
  let x = 0;
  let mut y = 1;
  
  let a = &x;
  let b = &y;
  let c = foo(a, b);
  
  y = 2; //~ ERROR cannot assign to `y` because it is borrowed
  let z = *c;
}
```
