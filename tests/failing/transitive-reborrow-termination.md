# Transitive reborrow termination

```rust
fn main() {
  let mut x = 0;
  let y = &mut x;
  let z = &mut *y;
  x = 1; //~ ERROR cannot assign to `x` because it is borrowed
  *z = 2;
}
```

Liveness for reborrows is non-trivial. The liveness of the borrow `y` should
also transitively affect `z`.
