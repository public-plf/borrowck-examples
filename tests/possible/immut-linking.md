# Linking through immutable references

```rust
fn foo<'a1, 'a2, 'a3, 'b1, 'b2, 'b3>
  ( x : &'a1 &'a2 mut &'a3 usize
  , y : &'b1 &'b2 mut &'b3 usize
  ) -> &'a1 usize {
  let _ = if true { x } else { y }; //~ lifetime may not live long enough
  //~^ lifetime may not live long enough
  &(**x)
}

fn main () {
}
```

This is another limitation of the unification rules for mutable borrows.
Ordinarily, unifying nested mutable borrows creates a "link" between them for a
good soundness reason - writing a new reference to the interior of the nested
borrow can affect the liveness information of both of the original borrows.

Here, however, the interiors are not actually mutable, and no write is possible.
Therefore, the unification of `x` and `y` does not need to have any effects on
the liveness information of `x` and `y` (unless the mutable references were
somehow under a type allowing interior mutability.)
