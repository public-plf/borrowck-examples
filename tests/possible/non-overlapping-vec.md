# Non-overlapping vector borrows

```rust
struct S {
    x1 : usize,
    x2 : usize,
}

fn main () {
    let mut v : Vec <S> = vec![S { x1: 0, x2: 1}]; 
    let y1 = &mut v[0].x1;
    let y2 = &mut v[0].x2; //~ ERROR cannot borrow `v` as mutable more than once at a time
    
    println!("{} {}", *y1, *y2);
}
```

This is an interesting limitation of the "array boundary" in borrow-checking -
the loan associated with `y1` is placed on the *whole* vector `v`, since the
compiler doesn't do index-disjointness reasoning for borrows. However, the
borrows of `y1`, `y2` don't conflict *regardless of index*, since they access
disjoint fields.

With some cleverness, this could be accepted by the compiler. The loans
associated with `y1`, `y2`, need to be associated with `v`, but still be marked
as being on subfields of entries of `v`.
