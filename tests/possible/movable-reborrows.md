# Movable reborrowed references

```rust
fn main() {
  let mut x = 0;
  let mut y = 1;
  
  let a = &mut x;
  let b = &mut y;
  
  let i = &mut *a;
  let j = &mut *b;
  
  let c = if false {
    // a moves
    a //~ ERROR cannot move out of `a` because it is borrowed
  } else {
    // b moves
    b
  };
  
  *i = 2;
  *c = 3;
}
```

Rust doesn't allow moving of any borrowed value, including reborrowed
references. In fact, references which are reborrowed are safe to move (though
not safe to pass to functions.)
