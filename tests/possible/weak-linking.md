# Lazier linking

```rust
fn foo<'a, 'b>(x : &'a mut &'b mut i32) {
  let mut a = 0i32;
  let mut b = &mut a; //~ ERROR `a` does not live long enough
  let c = &mut b;

  let _ = if true {
      x
  } else {
      c
  };
}

fn main () {}
```

This shows the limitations of Rust's type unification approach for nested
borrows, which "links" the region `'b` with the anonymous origin of the
reference in `b`. This happens because of the type unification of `x` and `c`,
even though no operation is done with this unified value.

This could be accepted by the compiler with some notion of "link activation" -
that is, if the compiler detects that an operation may be performed that
*requires* the origin of `b` to be bound below by the region `'b`. Such
operation would be a write; a function call using the unified reference; or
something else.
