# Types imply region constraints

```rust
fn foo<'a, 'b>(_x : &'a &'b i32, y : &'b i32) -> &'a i32{
  y
}

fn main() {}
```

The above function trivially borrow-checks with the lifetime constraint `'b : 'a`. The interesting component is that the *existence* of the type of `_x` is enough for the compiler to deduce this constraint.
