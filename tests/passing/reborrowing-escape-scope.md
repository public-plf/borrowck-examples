# Reborrow escaping scope

```rust
fn main() {
  let mut x : usize = 0;
  let z = {
    let y = &mut x;
    &mut *y
  };
  *z = 1;
}
```

Despite the descoping of `y`, the above code is still able to consider its
reborrow `z` as live, so the dereference borrow-checks.
