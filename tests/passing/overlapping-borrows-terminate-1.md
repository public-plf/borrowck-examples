# Overlapping borrows terminate early

```rust
fn main() {
    let mut x = (0, 1);
    let y = &mut x;
    let z = &mut x.0;
    *z = 2;
}
```
