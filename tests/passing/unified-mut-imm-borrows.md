# Unifying a value borrowed mutably and immutably

```rust
fn main() {
  let mut x : i32 = 0;
  let mut y : i32 = 1;
  let z : i32 = 2;

  let (v, w) = if true {
      (&mut x, &z)
  } else {
      (&mut y, &x)
  };

  println!("v: {}", *v);
  println!("x: {}", x);
  println!("w: {}", *w);
}
```

This is an example of a tricky case in branch unification where `x` is borrowed
immutably in one branch and mutably in another. The resulting mutable borrow `v`
is usable until terminated by the read from `x`, but the immutable borrow `w` is
usable *even past this point*, because in the case where it derives from `x`,
the borrow that created it was immutable, so the read from `x` was compatible
with that borrow.

This proves that a borrow-checker cannot simplify collapse the borrow-state of
`x` to be "mutably-loaned" when doing type unification, since otherwise `w`
would be made unusable by the read from `x` (which terminates the mutable loan.)
