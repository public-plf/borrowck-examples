# Self-reborrows inside loops

```rust
fn main() {
  let mut x = 0;
  let mut y = &mut x;

  for i in 1..100 {
      y = &mut *y;
  }

  *y = 1;
}
```
