# Reborrow overwriting

```rust
fn main() {
  let mut x = 0;
  let mut y = 1;
  
  let mut a = &mut x;
  let b = &mut y;
  
  let i = &mut *a;
  a = b;
  
  let w = *i;
}
```
