# Overlapping borrows terminate early

```rust
fn main() {
    let mut x = (0, 1);
    let y = &mut x.0;
    let z = &mut x;
    (*z).0 = 2;
}
```
