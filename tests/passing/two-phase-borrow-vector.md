# Two-phase borrowing

```rust
fn main() {
  let mut v : Vec<usize> = vec![0];
  v.push(v.len());
}
```

The prototypical example from the two-phase borrows RFC. A naive borrow-checker
prevents this code from compiling because `v` is borrowed mutably in the
receiver of `push` before it is borrowed immutably in the receiver of `len`.

However, with two-phase borrows, the code can be accepted, since the borrow by
`len` sits inside the reservation period of the borrow by `push`.
