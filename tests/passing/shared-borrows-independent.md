# Shared borrows are independent

```rust
fn main() {
  let x = 0;
  let y = 1;

  let mut a = &x;
  let b = &y;

  let c = &*a;
  a = b;

  let z = *c;
}
```
