# Mutable borrow over immutable reborrow

```rust
fn main() {
    let x : (Box<i32>, Box<i32>) = (Box::new(0i32), Box::new(1i32));
    let mut y = &x;
    let z = &(*y.1);
    let w = &mut y;
    println!("{}, {}", (*w).0, *z);
}
```

The immutable reborrow `z` of `y.1` is allowed to coincide with the mutable
borrow of the reference `y` itself by `w`. Therefore, `y` is effectively a
place which is mutably borrowed (by `w`) and immutably borrowed (by `z`).
This is allowed by the indirection of the nested immutable reference.
