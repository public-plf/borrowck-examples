# Fresh borrows inside loops

```rust
fn main() {
  let mut x = 0;
  let mut y = &mut x;
  while(*y < 10) {
      x = x + 1;
      y = &mut x;
  }
  *y = 0;
}
```

This example is typical of a Rust loop that has no equivalent translation
into a recursive function. Since `x` is borrowed when it enters the loop, it
could not be passed to a function that performs the loop body.
